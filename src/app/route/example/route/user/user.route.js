import { asyncComponent } from 'core';

import { store } from 'app/redux/store';

import User from 'bundle-loader?lazy!./user.component';
import { URL_USER, REDUCER_USER } from './user.constant';
import { userReducer } from './user.reducer';

/**
 * InjectReducer
 * @return {void}
 */
export const injectReducer = () => {
  store.injectReducer(REDUCER_USER, userReducer);
};

/**
 * Lazy load User component
 */
const component = asyncComponent({
  component: User,
  preProcess: injectReducer
});

/**
 * User route
 */
export const userRoute = {
  path: URL_USER,
  component
};
