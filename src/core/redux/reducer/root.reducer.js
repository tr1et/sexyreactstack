import { BaseReducer } from './base.reducer';

export class RootReducer extends BaseReducer {
  createSubReducers = subReducerClasses => {
    // eslint-disable-next-line new-cap
    this.subReducers = subReducerClasses.map(reducerClass => new reducerClass());
  };

  init() {
    this.createSubReducers(this.subReducerClasses);
  }

  getState = (state, action) => {
    if (this.canHandle(action.type)) {
      return this.getNewState(state, action);
    }

    for (const reducer of this.subReducers) {
      if (reducer.canHandle(action.type)) {
        return reducer.getNewState(state, action);
      }
    }

    // return current state
    return state;
  };
}
