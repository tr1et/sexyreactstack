/* URLs */
export const URL_APP = '/';
export const URL_HOME = '/';
export const URL_DASHBOARD = '/dashboard';
export const URL_ABOUT = '/about';

/* Reducers */
export const REDUCER_APP = 'app';

/* Actions */
export const ACTION_TOGGLE_SIDEBAR = 'ACTION_TOGGLE_SIDEBAR';
