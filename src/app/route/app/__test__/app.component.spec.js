import React from 'react';
import PropTypes from 'prop-types';

import { shallow } from 'test-util';
import App from '../app.component';

/**
* Wrapped App component because App has connect to Redux
*/
const AppComponent = App.WrappedComponent;

/**
* Test get propTypes
* @return {Void}
*/
const testPropTypes = () => {
  it('propTypes', () => {
    // arrange
    const expectedPropTypes = {
      route: PropTypes.object,
      auth: PropTypes.object
    };

    // expect
    assert.deepEqual(AppComponent.propTypes, expectedPropTypes);
  });
};

/**
* Test get model()
* @return {Void}
*/
const testModel = () => {
  it('model()', () => {
    // arrange
    const auth = {
      isAuthenticated: true
    };
    const route = {
      routes: [{ path: '/', component: null }]
    };
    const expectedModel = {
      auth,
      route
    };

    // sut
    const wrapper = shallow(<AppComponent auth={auth} route={route} />);
    const wrapperInstance = wrapper.instance();
    const {model} = wrapperInstance;

    // expect
    assert.deepEqual(model, expectedModel);
  });
};

describe('app.component.spec.js', () => {
  testPropTypes();
  testModel();
});
