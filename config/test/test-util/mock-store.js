import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { apiMiddleware, IMMUTABLE_EMPTY_MAP } from 'core';

export const createMockStore = (initialState = IMMUTABLE_EMPTY_MAP, middlewares = [thunk, apiMiddleware]) => {
  const mockStore = configureMockStore(middlewares);

  return mockStore(initialState);
};
