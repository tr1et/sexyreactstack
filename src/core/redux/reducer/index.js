import { combineReducers } from 'redux-immutable';

import { routerReducer } from './router.reducer';

/**
* Create root reducer
* @param  {Object} reducers {reducers}
* @return {type} {description}
*/
export const createReducer = (reducers) => {
  return combineReducers({
    routing: routerReducer,
    ...reducers
  });
};

export * from './router.reducer';
export * from './base.reducer';
export * from './root.reducer';