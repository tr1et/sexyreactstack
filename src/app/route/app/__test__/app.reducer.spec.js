import Immutable from 'immutable';
import { given } from 'mocha-testdata';
import { CLIENT_INIT } from 'redux/store';

import { Screen } from 'core';

import { ACTION_TOGGLE_SIDEBAR } from '../app.constant';
import { appReducer } from '../app.reducer';

/**
 * Test the CLIENT_INIT case
 *
 * @return {Void}
 */
const testHandleClientInit = () => {
  context('CLIENT_INIT()', () => {
    it('Should init isSidebarOpen correctly', () => {
      // arrange
      const isDesktop = true;
      const initialState = Immutable.fromJS({
        isSidebarOpen: false
      });
      const expectedState = {
        isSidebarOpen: isDesktop
      };

      sinon.stub(Screen, 'isDesktop');
      Screen.isDesktop.returns(isDesktop);

      // sut
      const newState = appReducer(initialState, {type: CLIENT_INIT});

      // expect
      assert.deepEqual(newState.toJS(), expectedState);

      // reset
      Screen.isDesktop.restore();
    });
  });
};

/**
 * Test the ACTION_TOGGLE_SIDEBAR case
 *
 * @return {Void}
 */
const testHandleToggleSidebar = () => {
  context('ACTION_TOGGLE_SIDEBAR', () => {
    given({
      initialIsSidebarOpen: false,
      inputPayload: {
        isSidebarOpen: false
      },
      expectedIsSidebarOpen: false
    },
    {
      initialIsSidebarOpen: false,
      inputPayload: {
        isSidebarOpen: true
      },
      expectedIsSidebarOpen: true
    },
    {
      initialIsSidebarOpen: true,
      inputPayload: {
        isSidebarOpen: false
      },
      expectedIsSidebarOpen: false
    },
    {
      initialIsSidebarOpen: true,
      inputPayload: {
        isSidebarOpen: true
      },
      expectedIsSidebarOpen: true
    },
    {
      initialIsSidebarOpen: false,
      inputPayload: {},
      expectedIsSidebarOpen: true
    },
    {
      initialIsSidebarOpen: true,
      inputPayload: {},
      expectedIsSidebarOpen: false
    },
    {
      initialIsSidebarOpen: false,
      inputPayload: {
        isSidebarOpen: null
      },
      expectedIsSidebarOpen: true
    },
    {
      initialIsSidebarOpen: true,
      inputPayload: {
        isSidebarOpen: null
      },
      expectedIsSidebarOpen: false
    })
    .it('Should change isSidebarOpen correctly', ({
      initialIsSidebarOpen,
      inputPayload,
      expectedIsSidebarOpen
    }) => {
      // arrange
      const initialState = Immutable.fromJS({
        isSidebarOpen: initialIsSidebarOpen
      });
      const expectedState = {
        isSidebarOpen: expectedIsSidebarOpen
      };

      // sut
      const newState = appReducer(initialState, {
        type: ACTION_TOGGLE_SIDEBAR,
        payload: inputPayload
      });

      // expect
      assert.deepEqual(newState.toJS(), expectedState);
    });
  });
};

/**
 * Test the default case
 *
 * @return {Void}
 */
const testDefaultCase = () => {
  context('default', () => {
    given({
      input: Immutable.fromJS({
        a: 'abc',
        b: 234
      }),
      expected: {
        a: 'abc',
        b: 234
      }
    },
    {
      input: undefined,
      expected: {
        error: null,
        isLoading: false,
        isSidebarOpen: false
      }
    })
    .it('Should return original state', ({ input, expected }) => {
      // sut
      const newState = appReducer(input);

      // expect
      assert.deepEqual(newState.toJS(), expected);
    });
  });
};

// Unit tests for AppReducer
describe('app.reducer.spec.js', () => {
  testHandleClientInit();
  testHandleToggleSidebar();
  testDefaultCase();
});
