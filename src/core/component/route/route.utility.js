import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { Switch, Route } from 'react-router-dom';

/**
 * Transition options when change route
 * @return {Object} Transition options
 */
const transitionOptions = {
  transitionName: 'fade',
  transitionAppear: true,
  transitionEnter: true,
  transitionLeave: true,
  transitionAppearTimeout: 300,
  transitionEnterTimeout: 300,
  transitionLeaveTimeout: 300
};

/**
 * Render component with animation when route changed
 * @param  {Object} route     Routing info we registered { path, exact, component, routes }
 * @param  {Object} props     Route's props
 * @return {Component}        Route component wrapped in animated transition component
 */
export const renderAnimatedComponent = (route, props) => {
  props = props || {};

  if (route && route.component) {
    return (
      <CSSTransitionGroup {...transitionOptions}>
        <route.component {...props} route={route} />
      </CSSTransitionGroup>
    );
  }

  return null;
};

/**
 * Render registered routes with animated transition
 * @param  {Array} routes     Array of object route { path, exact, component, routes }
 * @return {Component}        Route components wrapped in Switch
 */
export const renderRoutes = (routes) => {
  return routes
    ? <Switch>
        {
          routes.map((route, i) => (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              strict={route.strict}
              render={renderAnimatedComponent.bind(this, route)}
            />
          ))
        }
      </Switch>
    : null;
};