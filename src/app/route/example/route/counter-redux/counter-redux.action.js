import { ACTION_INCREASE, ACTION_DECREASE } from './counter-redux.constant';

/**
 * Increase counter
 * @return {func} Dispatch an action to increase counter
 */
export const increase = () => {
  return dispatch => dispatch({
    type: ACTION_INCREASE
  });
};

/**
 * Decrease counter
 * @return {func} Dispatch an action to decrease counter
 */
export const decrease = () => {
  return dispatch => dispatch({
    type: ACTION_DECREASE
  });
};
